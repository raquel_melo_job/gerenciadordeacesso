package com.itau.GerenciadorAcesso.producer;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.Random;

public class Passe {

    private String nomeCliente;
    private String numPorta;
    private boolean autoriza;

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public String getNumPorta() {
        return numPorta;
    }

    public void setNumPorta(String numPorta) {
        this.numPorta = numPorta;
    }

    public boolean isAutoriza() {
        return autoriza;
    }

    public void setAutoriza() {
        Random random = new Random();
        this.autoriza = random.nextBoolean();
    }
}
