package com.itau.GerenciadorAcesso.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class PasseProducer {

    @Autowired
    private KafkaTemplate<String,Passe> producer;

    public void solicitarAcesso(Passe passe) {
        producer.send("spec2-raquel-batista-1", passe);
    }
}
