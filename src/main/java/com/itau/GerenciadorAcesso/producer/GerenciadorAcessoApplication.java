package com.itau.GerenciadorAcesso.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GerenciadorAcessoApplication {

	public static void main(String[] args) {
		SpringApplication.run(GerenciadorAcessoApplication.class, args);
	}

}
