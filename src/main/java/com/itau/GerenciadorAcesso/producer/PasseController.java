package com.itau.GerenciadorAcesso.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PasseController {

    @Autowired
    private PasseProducer passeProducer;

    @PostMapping("/acesso/{nome}/{porta}")
    public void create(@PathVariable String nome, @PathVariable String porta){
        Passe passe = new Passe();
        passe.setNomeCliente(nome);
        passe.setNumPorta(porta);
        passe.setAutoriza();
        passeProducer.solicitarAcesso(passe);
    }
}
